package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {
    public static void main(String args[]) {
        Connection c = null;
        Statement stmt = null;
        Statement stmt2 = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "postgres", "aliarman01");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            stmt2 = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Student;" );
            while ( rs.next() ) {
                Student student = new Student(rs.getInt("id"), rs.getString("name"), rs.getString("phone"), rs.getInt("groupid"));
                ResultSet rs2 = stmt2.executeQuery("SELECT * FROM GroupS WHERE id = "+ student.getGroupId() +";");
                Group group = new Group();
                while ( rs2.next() ) {
                    group.setId(rs2.getInt("id"));
                    group.setName(rs2.getString("name"));
                }
                System.out.print(student);
                System.out.println(group);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        } finally {

        }

    }
}
